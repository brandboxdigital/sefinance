// var gulp       = require('gulp'),
//     // browserify = require('browserify'),
//     // tap        = require('gulp-tap'),
//     // source     = require('vinyl-source-stream'),
//     // buffer     = require('vinyl-buffer'),
//     minify     = require("gulp-babel-minify"),
//     // babelify   = require('babelify'),
//     sourcemaps = require('gulp-sourcemaps'),
//     concat     = require('gulp-concat');

// var autoprefix  = require("gulp-autoprefixer"),
//     browserSync = require('browser-sync').create(),    
//     sass        = require("gulp-sass"),
//     bourbon     = require("bourbon").includePaths;
//     // neat        = require("bourbon-neat").includePaths;

// var twig = require('gulp-twig');






/* ==================================================== */
var log         = require('fancy-log');
/* ==================================================== */
var gulp        = require('gulp');
var sass        = require('gulp-sass');
var concat      = require('gulp-concat');
var babel       = require("gulp-babel");
var browserSync = require('browser-sync').create();
var bourbon     = require("bourbon").includePaths;
var autoprefix  = require("gulp-autoprefixer");
var sourcemaps  = require('gulp-sourcemaps');
var minify      = require("gulp-babel-minify");
var svgSprite   = require("gulp-svg-sprites");
var twig        = require('gulp-twig');
var del         = require('del');

var sassPaths = [
  'assets/scss/',
  bourbon
];

var jsPaths = [
  'node_modules/@babel/polyfill/dist/polyfill.js',
  'assets/js/modules/**/*.js', 
  'assets/js/main.js',
];

var svgconfig = {
  cssFile: "scss/basic/_svg-sprites.scss",
  svg: {
      sprite: "img/icon-sprite.svg",
      symbols: "img/icon-sprite.svg",
      defs: "img/icon-sprite.svg",
  },
  preview: {
      sprite: "img/icon-sprite-preview.html",
      symbols: "img/icon-sprite-preview.html",
      defs: "img/icon-sprite-preview.html",
  },

  mode: "symbols",
};

var buildpath = './_build/';

var destPath = {
  js:   buildpath+'assets/js/min',
  css:  buildpath+'assets/css',
  html: buildpath+'',
}

/**
 * ===============================================================
 * Compiling svg sprites
 */
gulp.task('sprites', function () {
    return gulp.src('assets/img/icon-sprite-saurce/*.svg')
      .pipe(svgSprite(svgconfig))
      .pipe(gulp.dest("assets"));
});

/**
 * ===============================================================
 * Compiling sass files
 */
gulp.task("sass-build--env-dev", function () {
  return gulp.src('assets/scss/main.scss')
    .pipe(sass({
          sourcemaps: true,
          includePaths: sassPaths,
      })
      .on('error', sass.logError)
    )
    .pipe(autoprefix("last 2 versions"))
    .pipe(gulp.dest(destPath.css))
    .pipe(browserSync.stream());
});

gulp.task("sass-build", function () {
  return gulp.src('assets/scss/main.scss')
    .pipe(sass({
          sourcemaps: true,
          includePaths: sassPaths,
          outputStyle: 'compressed',
      })
      .on('error', sass.logError)
    )
    .pipe(autoprefix("last 4 versions"))
    .pipe(gulp.dest(destPath.css))
});

/**
 * ===============================================================
 * Compiling js files and running then trough babel
 */
gulp.task('js-vendor', function(done) {
  return gulp.src(['assets/js/vendor/*.js'])
    .pipe(concat('vendor.js'))
    // .pipe(babel({presets: ["@babel/env"]}))
    .pipe(gulp.dest(destPath.js))
    .pipe(browserSync.stream());
});

gulp.task('js-build--env-dev', function(done) {
  return gulp.src(jsPaths)
    .pipe(concat('main.js'))
    // .pipe(babel({presets: ["@babel/env"]}))
    .pipe(gulp.dest(destPath.js))
    .pipe(browserSync.stream());
});

gulp.task('js-build', function() {

  return gulp.src(jsPaths)
    .pipe(concat('main.js'))
    // .pipe(babel({presets: ["@babel/env"]}))
    .pipe(sourcemaps.init())
    .pipe(minify({
      mangle: {
        keepClassName: true,
        keepFnName: true
      }
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(destPath.js))
});


/**
 * ======================
 * HTML + TWIG for static sites...
 */
gulp.task('html-compile', function () {
    
    return gulp.src('twig/pages/*.twig')
      .pipe(twig({
          base: './twig'
      }))
      .pipe(gulp.dest(destPath.html))
      .pipe(browserSync.stream());
});

gulp.task('copy', function () {
    return gulp.src([
      './assets/**',
      '!./assets/scss/**',
      '!./assets/js/**',
      '!./assets/css/**',
    ])
    .pipe(gulp.dest(buildpath+'assets'));
});

gulp.task('clearbuild', function () {
    return del([buildpath]);
    // return gulp.src(buildpath, {read: false})
//     .pipe(clean())
});

/**
 * ===============================================================
 * Calling Browser Sync And watching for changes
 */
gulp.task('watcher', function() {
  browserSync.init({
      server: {
          baseDir: destPath
      }
      // proxy: "demo.vagrant.test",
  });

  gulp.watch(['assets/img/icon-sprite-saurce/*.svg'], gulp.series(['sprites']));

  gulp.watch(['assets/scss/**/*.scss'], gulp.series(['sass-build--env-dev']));

  gulp.watch(['assets/js/vendor/**/*.js'], gulp.series(['js-vendor']));
  gulp.watch(['assets/js/main.js','assets/js/modules/**/*.js'], gulp.series(['js-build--env-dev']));
  
  gulp.watch(['twig/**/*.twig'], gulp.series(['html-compile']));
  // gulp.watch(['**/*.html', '**/*.htm']).on('change', browserSync.reload);

  gulp.watch([
      './assets/**',
      '!./assets/scss/**',
      '!./assets/js/**',
      '!./assets/css/**',
    ], gulp.series(['copy']));
});


gulp.task( "default", gulp.series([ 'clearbuild', 'sprites', 'copy', 'sass-build--env-dev', 'js-vendor', 'js-build--env-dev', 'html-compile', 'watcher']) );
gulp.task( "build", gulp.series([ 'clearbuild', 'sprites', 'copy', 'sass-build', 'js-vendor', 'html-compile', 'js-build']) );

