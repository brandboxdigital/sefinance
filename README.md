## Installation

1. Clone the repository, and open a shell terminal within the project's root
   directory.

1. After [installing Node.js and NPM], install the gulp via the following.

   ```
   npm install --global gulp-cli
   ```

1. Install the project packages.

   ```
   npm install
   ```

1. Run the default `gulp` command which will complie the stylesheets, with
   Bourbon inclded as well as creating a local server at
   [`http://localhost:3000`](http://localhost:3000). Gulp will continue to watch
   the `/stylesheets` to monitor for any changes.

   ```
   gulp
   ```

1. Run `gulp build` command to minify all files.
   ```
   gulp build
   ```

1. Check the `_build` derectory for final html, css and js files.
