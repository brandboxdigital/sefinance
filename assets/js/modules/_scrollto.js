ScrollTo = function (id) {
    var me = this;
    
    me.id = id || '[data-scroll-to]';

    me.init = function () {
        $('body').on('click', me.id, me.clickon);
    }

    me.clickon = function(e) {
        e.preventDefault();

        var id = $(this).data('scroll-to');
        var $el = $(id);
        
        $("html, body").stop().animate({
            scrollTop:$el.offset().top
        }, 500);
    }

    me.init();
};
