Tabs = function (id) {
    var me = this;

    me.visibility_class = 'is-active';
    me.initiated_class  = 'is-initiated';

    me.id = id || '[data-tabbed]';
    me.resize_handler = null;

    me.init = function () {
        var tabs = $(me.id);

        me._calculataMaxHeight(tabs)
        me.switchTab.call($(me.id+' .tabs__header .is-active'), new Event('click'));
        
        tabs.on('click', '[data-tabbed-id]', me.switchTab);
        
        tabs.on('click', '.tabs__header .move--left', me.switchTab_prev);
        tabs.on('click', '.tabs__header .move--right', me.switchTab_next);

        $(window).on('resize', me.resize);

        tabs.addClass(me.initiated_class);
    }

    me.switchTab = function(e) {
        // Maybe PreventDefault would be goo idea
        e.preventDefault();
        
        var id = $(this).data('tabbed-id');
        
        $(this).siblings('li').removeClass(me.visibility_class);
        $(this).addClass(me.visibility_class);

        me._fixNextPrevHeaderCalsses(this);

        me._switchBody(id);
    }

    me.switchTab_next = function(e) {
        // Maybe PreventDefault would be goo idea
        e.preventDefault();

        var next = $(this).siblings('li.next');
        var cur = $(this).siblings('.'+me.visibility_class);

        $('.tabs__header_list .will-animate').removeClass('will-animate');
        next.addClass('will-animate');
        cur.addClass('will-animate');

        me._fixNextPrevHeaderCalsses(next);

        var id = next.data('tabbed-id');
        me._switchBody(id);
    }

    me.switchTab_prev = function(e) {
        // Maybe PreventDefault would be goo idea
        e.preventDefault();

        var prev = $(this).siblings('li.prev');
        var cur = $(this).siblings('.'+me.visibility_class);

        $('.tabs__header_list .will-animate').removeClass('will-animate');
        prev.addClass('will-animate');
        cur.addClass('will-animate');

        me._fixNextPrevHeaderCalsses(prev);

        
        var id = prev.data('tabbed-id');
        me._switchBody(id);
    }


    me._fixNextPrevHeaderCalsses = function(curent) {
        var all  = $(me.id + ' .tabs__header_list li');
        var next = $(curent).next('li');
        var prev = $(curent).prev('li');

        all.removeClass('next prev '+me.visibility_class);
        $(curent).addClass(me.visibility_class);
        
        if (next.length > 0) {
            next.addClass('next');
        } else {
            all.first().addClass('next');
        }

        if (prev.length > 0) {
            prev.addClass('prev');
        } else {
            all.last().addClass('prev');
        }
    }
    me._fixNextPrevBodyCalsses = function(curent) {
        var all  = $(me.id + ' .tabs__body_list li');
        var next = $(curent).next('li');
        var prev = $(curent).prev('li');

        all.removeClass('next prev '+me.visibility_class);
        $(curent).addClass(me.visibility_class);
        
        if (next.length > 0) {
            next.addClass('next');
        } else {
            all.first().addClass('next');
        }

        if (prev.length > 0) {
            prev.addClass('prev');
        } else {
            all.last().addClass('prev');
        }
    }

    me._switchBody = function(id) {
        var tabs = $(me.id);

        $target = $("#"+id);
        $current = tabs.find('.tabs__body_list .'+me.visibility_class)

        $('.tabs__body_list .will-animate').removeClass('will-animate');
        $target.addClass('will-animate');
        $current.addClass('will-animate');

        if ($target.length > 0) {
            me._fixNextPrevBodyCalsses($target);
            $target.parent().height($target.height());
        }
    }

    me._calculataMaxHeight = function(tabs) {
        var body_max_height = 0;

        tabs.find('.tabs__body_list li').each(function(i, el) {
            var h = $(el).height();

            body_max_height = (h > body_max_height)
                ? h
                : body_max_height;
        })

        var header_height = tabs.find('.tabs__header').height();

        tabs.css('height', body_max_height + header_height);
    }

    me.resize = function() {
        clearTimeout(me.resize_handler);
        
        me.resize_handler = setTimeout(function(me) {
            me._calculataMaxHeight($(me.id));

            me.switchTab.call($('.tabs__header .is-active'), new Event('click'));

            me.resize_handler = null;
        }, 50, me);
    }

    me.init();
};
