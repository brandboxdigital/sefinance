Modal = function (id) {
    var me = this;

    me.visibility_class = 'is-visible';
    me.id = id || '.modal';

    me.init = function () {
        var modals = $(me.id);
        
        modals.on('click', '[data-close]', me.closeModal)

        // register all open event DOM elements
        modals.each(function(i,el){
            var cssclass = "[data-open-modal='"+el.id+"']";
            $(document).on('click', cssclass, me.openModal);
        })
    }

    me.openModal = function(e) {
        // Maybe PreventDefault would be goo idea
        e.preventDefault();
        
        var id = $(this).data('open-modal');
        
        try {
            $("#"+id).addClass(me.visibility_class)
        } catch(e) {
            console.error(e);
        }
    }
    me.closeModal = function() {
        $(this).closest('.modal').removeClass(me.visibility_class);
    }

    me.init();
};
